SHOW autovacuum;
DROP TABLE IF EXISTS t;
CREATE TABLE t(k serial PRIMARY KEY,v integer);
INSERT INTO t(v) 
  SELECT trunc(random() * 10)
    FROM generate_series(1,100000);
SELECT relname,relpages,reltuples FROM pg_class WHERE relname='t';
VACUUM ANALYZE t;
SELECT relname,relpages,reltuples FROM pg_class WHERE relname='t';
SELECT relname,round(reltuples / relpages) AS rows_per_page FROM pg_class WHERE relname='t';

CREATE OR REPLACE VIEW table_stats AS
SELECT
  stat.relname AS relname, 
  seq_scan, seq_tup_read, idx_scan, idx_tup_fetch,
  heap_blks_read, heap_blks_hit, idx_blks_read, idx_blks_hit    
FROM
  pg_stat_user_tables stat
  RIGHT JOIN pg_statio_user_tables statio 
    ON stat.relid=statio.relid
;
-- Stats on all of the above takes a bit to settle down
SELECT pg_sleep(10);
SELECT pg_stat_reset();

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

EXPLAIN ANALYZE SELECT count(*) FROM t WHERE k=1000;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

EXPLAIN ANALYZE SELECT count(*) FROM t WHERE v=1;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

EXPLAIN ANALYZE SELECT count(*) FROM t WHERE k>9000 AND v=5;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

CREATE INDEX i ON t(v);
SELECT relname,reltuples,relpages FROM pg_class WHERE relname='i';
SELECT relname,round(reltuples / relpages) AS rows_per_page FROM pg_class WHERE relname='i';

EXPLAIN ANALYZE SELECT count(*) FROM t WHERE v=1;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

EXPLAIN ANALYZE SELECT count(*) FROM t WHERE k>9000 AND v=5;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off


-- At this point, we haven't done an ANALYZE since the index was
-- created.

EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<1;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<2;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<3;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<4;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<5;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<6;
SELECT pg_sleep(1);
SELECT pg_stat_reset();

EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<4;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<6;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

-- ANALYZE with the index in place and repeat
ANALYZE t;

EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<1;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<2;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<3;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<4;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<5;
EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<6;
SELECT pg_stat_reset();

EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<4;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<6;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

-- Reorganize table based on the values
CLUSTER t USING i;
ANALYZE t;
SELECT pg_sleep(1);
SELECT pg_stat_reset();

EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<4;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

EXPLAIN ANALYZE SELECT COUNT(*) FROM t WHERE v<6;

SELECT pg_sleep(1);
\pset x on
SELECT * FROM table_stats WHERE relname='t';
SELECT pg_stat_reset();
\pset x off

-- Exercise PostgreSQL 9.0 specific feature
EXPLAIN (ANALYZE ON, BUFFERS ON) SELECT count(*) FROM t WHERE v=5;
DROP INDEX i;
EXPLAIN (ANALYZE ON, BUFFERS ON) SELECT count(*) FROM t WHERE v=5;
