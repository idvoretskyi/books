#!/bin/bash

# Print table statistics for a table

if [ -z "$1" ]; then
  echo "Usage: table-stats.sh table [db]"
  exit 1
fi

TABLENAME="$1"
if [ -n "$2" ] ; then
  DB="-d $2"
fi

PSQL="psql $DB -x -c "

# Print basic configuration information
$PSQL "
SELECT
  tablename,attname,null_frac,avg_width,n_distinct,correlation,
  most_common_vals,most_common_freqs,histogram_bounds
FROM pg_stats
WHERE tablename='$TABLENAME';
" | grep -v "\-\[ RECORD "

