-- Basic table size query
SELECT 
  nspname,
  relname,
  pg_size_pretty(pg_relation_size(C.oid)) AS size
FROM pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
WHERE nspname NOT IN ('pg_catalog', 'information_schema')
ORDER BY pg_relation_size(C.oid) DESC
LIMIT 20;

-- PostgreSQL 9.0 only size query including TOAST
SELECT 
  nspname,
  relname,
  relkind as "type",
  pg_size_pretty(pg_table_size(C.oid)) AS size,
  pg_size_pretty(pg_indexes_size(C.oid)) AS idxsize,
  pg_size_pretty(pg_total_relation_size(C.oid)) as "total"
FROM pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
WHERE nspname NOT IN ('pg_catalog', 'information_schema') AND
  nspname !~ '^pg_toast' AND
  relkind IN ('r','i')
ORDER BY pg_total_relation_size(C.oid) DESC
LIMIT 20;

-- Compute TOAST and index sizes manually for earlier versions
SELECT
  nspname,
  C.relname,
  C.relkind as "type",
  pg_size_pretty(pg_relation_size(C.oid)) AS size,
  pg_size_pretty(
    CASE when C.reltoastrelid > 0 THEN pg_relation_size(C.reltoastrelid) ELSE 0 END +
    CASE when T.reltoastidxid > 0 THEN pg_relation_size(T.reltoastidxid) ELSE 0 END
    ) AS toast,
  pg_size_pretty(cast(
    (SELECT sum(pg_relation_size(I.indexrelid))
     FROM pg_index I WHERE I.indrelid = C.oid)
     AS int8)) AS idxsize,
  pg_size_pretty(pg_total_relation_size(C.oid)) as "total"
FROM pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
LEFT OUTER JOIN pg_class T ON (C.reltoastrelid=T.oid)
WHERE nspname NOT IN ('pg_catalog', 'information_schema') AND
  nspname !~ '^pg_toast' AND
  C.relkind IN ('r','i')
ORDER BY pg_total_relation_size(C.oid) DESC
LIMIT 20;

