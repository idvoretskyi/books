-- Samples of derived data from the various pg_stat* views

-- Index vs. sequential scan percentages
SELECT
  schemaname,
  relname,
  seq_scan,
  idx_scan,
  cast(idx_scan AS numeric) / (idx_scan + seq_scan) AS idx_scan_pct
FROM pg_stat_user_tables
WHERE (idx_scan + seq_scan)>0
ORDER BY idx_scan_pct;

-- Index vs. sequential scan tuples fetched
SELECT
  schemaname,
  relname,
  seq_tup_read,
  idx_tup_fetch,
  cast(idx_tup_fetch AS numeric) / (idx_tup_fetch + seq_tup_read) AS idx_tup_pct
FROM pg_stat_user_tables
WHERE (idx_tup_fetch + seq_tup_read)>0
ORDER BY idx_tup_pct;

-- HOT vs. regular UPDATE ratio
SELECT
  schemaname,
  relname,
  n_tup_upd,
  n_tup_hot_upd,
  cast(n_tup_hot_upd AS numeric) / n_tup_upd AS hot_pct
FROM pg_stat_user_tables
WHERE n_tup_upd>0
ORDER BY hot_pct;

-- Balance of INSERT vs. UPDATE vs. DELETE operations
SELECT
  schemaname,
  relname,
  cast(n_tup_ins AS numeric) / (n_tup_ins + n_tup_upd + n_tup_del) AS ins_pct,
  cast(n_tup_upd AS numeric) / (n_tup_ins + n_tup_upd + n_tup_del) AS upd_pct,
  cast(n_tup_del AS numeric) / (n_tup_ins + n_tup_upd + n_tup_del) AS del_pct
FROM pg_stat_user_tables ORDER BY relname;

-- Cached reads vs. OS reads
SELECT
  schemaname,
  relname,
  cast(heap_blks_hit as numeric) / (heap_blks_hit + heap_blks_read) AS hit_pct,
  heap_blks_hit,heap_blks_read
FROM pg_statio_user_tables
WHERE (heap_blks_hit + heap_blks_read)>0
ORDER BY hit_pct;

-- Cached index reads vs. OS reads
SELECT
  schemaname,
  relname,
  cast(idx_blks_hit as numeric) / (idx_blks_hit + idx_blks_read) AS hit_pct,
  idx_blks_hit,idx_blks_read
FROM pg_statio_user_tables
WHERE (idx_blks_hit + idx_blks_read)>0
ORDER BY hit_pct;

-- Totals for statio that include TOAST data
SELECT
 *,
 (heap_blks_read + toast_blks_read + tidx_blks_read) AS total_blks_read,
 (heap_blks_hit + toast_blks_hit + tidx_blks_hit) AS total_blks_hit
FROM pg_statio_user_tables;

-- Average row count returned by index scans
SELECT
  schemaname,
  relname,
  indexrelname,
  cast(idx_tup_read AS numeric) / idx_scan AS avg_tuples,
  idx_scan,idx_tup_read
FROM pg_stat_user_indexes
WHERE idx_scan > 0; 

-- Unused indexes
SELECT
  schemaname,
  relname,
  indexrelname,
  idx_scan,
  pg_size_pretty(pg_relation_size(i.indexrelid)) AS index_size
FROM
  pg_stat_user_indexes i
  JOIN pg_index USING (indexrelid)
WHERE
  indisunique IS false
ORDER BY idx_scan,relname;

-- Individual index cached reads vs. OS reads
SELECT
  schemaname,
  relname,
  indexrelname,
  cast(idx_blks_hit as numeric) / (idx_blks_hit + idx_blks_read) AS hit_pct,
  idx_blks_hit,idx_blks_read
FROM pg_statio_user_indexes
WHERE (idx_blks_hit + idx_blks_read)>0
ORDER BY hit_pct;

