--
-- Table partitioning example
--
-- Presumes you have already loaded the Dell Store 2 data for PostgreSQL.
-- Partitions up the orders table and migrates the existing data into
-- the partitioned structure.
--
-- Sample session:
--
-- createdb dellstore2
-- psql -d dellstore2 -f dellstore2-normal-1.0.sql
-- psql -d dellstore2 -f partitioning.sql
--

--
-- Collect statistics about the table
--

SELECT min(orderdate),max(orderdate) FROM orders;
SELECT relpages FROM pg_class WHERE relname='orders';

--
-- Partition orders table
--

CREATE TABLE orders_2004_01 (
  CHECK ( orderdate >= DATE '2004-01-01' and orderdate < DATE '2004-02-01')
) INHERITS (orders);
CREATE TABLE orders_2004_02 (
  CHECK ( orderdate >= DATE '2004-02-01' and orderdate < DATE '2004-03-01')
) INHERITS (orders);
CREATE TABLE orders_2004_03 (
  CHECK ( orderdate >= DATE '2004-03-01' and orderdate < DATE '2004-04-01')
) INHERITS (orders);
CREATE TABLE orders_2004_04 (
  CHECK ( orderdate >= DATE '2004-04-01' and orderdate < DATE '2004-05-01')
) INHERITS (orders);
CREATE TABLE orders_2004_05 (
  CHECK ( orderdate >= DATE '2004-05-01' and orderdate < DATE '2004-06-01')
) INHERITS (orders);
CREATE TABLE orders_2004_06 (
  CHECK ( orderdate >= DATE '2004-06-01' and orderdate < DATE '2004-07-01')
) INHERITS (orders);
CREATE TABLE orders_2004_07 (
  CHECK ( orderdate >= DATE '2004-07-01' and orderdate < DATE '2004-08-01')
) INHERITS (orders);
CREATE TABLE orders_2004_08 (
  CHECK ( orderdate >= DATE '2004-08-01' and orderdate < DATE '2004-09-01')
) INHERITS (orders);
CREATE TABLE orders_2004_09 (
  CHECK ( orderdate >= DATE '2004-09-01' and orderdate < DATE '2004-10-01')
) INHERITS (orders);
CREATE TABLE orders_2004_10 (
  CHECK ( orderdate >= DATE '2004-10-01' and orderdate < DATE '2004-11-01')
) INHERITS (orders);
CREATE TABLE orders_2004_11 (
  CHECK ( orderdate >= DATE '2004-11-01' and orderdate < DATE '2004-12-01')
) INHERITS (orders);
CREATE TABLE orders_2004_12 (
  CHECK ( orderdate >= DATE '2004-12-01' and orderdate < DATE '2005-01-01')
) INHERITS (orders);

ALTER TABLE ONLY orders_2004_01
    ADD CONSTRAINT orders_2004_01_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_02
    ADD CONSTRAINT orders_2004_02_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_03
    ADD CONSTRAINT orders_2004_03_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_04
    ADD CONSTRAINT orders_2004_04_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_05
    ADD CONSTRAINT orders_2004_05_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_06
    ADD CONSTRAINT orders_2004_06_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_07
    ADD CONSTRAINT orders_2004_07_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_08
    ADD CONSTRAINT orders_2004_08_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_09
    ADD CONSTRAINT orders_2004_09_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_10
    ADD CONSTRAINT orders_2004_10_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_11
    ADD CONSTRAINT orders_2004_11_pkey PRIMARY KEY (orderid);
ALTER TABLE ONLY orders_2004_12
    ADD CONSTRAINT orders_2004_12_pkey PRIMARY KEY (orderid);

CREATE INDEX ix_orders_2004_01_custid ON orders_2004_01 USING btree (customerid);
CREATE INDEX ix_orders_2004_02_custid ON orders_2004_02 USING btree (customerid);
CREATE INDEX ix_orders_2004_03_custid ON orders_2004_03 USING btree (customerid);
CREATE INDEX ix_orders_2004_04_custid ON orders_2004_04 USING btree (customerid);
CREATE INDEX ix_orders_2004_05_custid ON orders_2004_05 USING btree (customerid);
CREATE INDEX ix_orders_2004_06_custid ON orders_2004_06 USING btree (customerid);
CREATE INDEX ix_orders_2004_07_custid ON orders_2004_07 USING btree (customerid);
CREATE INDEX ix_orders_2004_08_custid ON orders_2004_08 USING btree (customerid);
CREATE INDEX ix_orders_2004_09_custid ON orders_2004_09 USING btree (customerid);
CREATE INDEX ix_orders_2004_10_custid ON orders_2004_10 USING btree (customerid);
CREATE INDEX ix_orders_2004_11_custid ON orders_2004_11 USING btree (customerid);
CREATE INDEX ix_orders_2004_12_custid ON orders_2004_12 USING btree (customerid);

ALTER TABLE ONLY orders_2004_01
    ADD CONSTRAINT fk_2004_01_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_02
    ADD CONSTRAINT fk_2004_02_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_03
    ADD CONSTRAINT fk_2004_03_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_04
    ADD CONSTRAINT fk_2004_04_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_05
    ADD CONSTRAINT fk_2004_05_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_06
    ADD CONSTRAINT fk_2004_06_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_07
    ADD CONSTRAINT fk_2004_07_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_08
    ADD CONSTRAINT fk_2004_08_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_09
    ADD CONSTRAINT fk_2004_09_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_10
    ADD CONSTRAINT fk_2004_10_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_11
    ADD CONSTRAINT fk_2004_11_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;
ALTER TABLE ONLY orders_2004_12
    ADD CONSTRAINT fk_2004_12_customerid FOREIGN KEY (customerid) REFERENCES customers(customerid) ON DELETE SET NULL;

CREATE OR REPLACE FUNCTION orders_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF    ( NEW.orderdate >= DATE '2004-12-01' AND
         NEW.orderdate < DATE '2005-01-01' ) THEN
        INSERT INTO orders_2004_12 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-11-01' AND
         NEW.orderdate < DATE '2004-12-01' ) THEN
        INSERT INTO orders_2004_11 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-10-01' AND
         NEW.orderdate < DATE '2004-11-01' ) THEN
        INSERT INTO orders_2004_10 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-09-01' AND
         NEW.orderdate < DATE '2004-10-01' ) THEN
        INSERT INTO orders_2004_09 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-08-01' AND
         NEW.orderdate < DATE '2004-09-01' ) THEN
        INSERT INTO orders_2004_08 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-07-01' AND
         NEW.orderdate < DATE '2004-08-01' ) THEN
        INSERT INTO orders_2004_07 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-06-01' AND
         NEW.orderdate < DATE '2004-07-01' ) THEN
        INSERT INTO orders_2004_06 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-05-01' AND
         NEW.orderdate < DATE '2004-06-01' ) THEN
        INSERT INTO orders_2004_05 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-04-01' AND
         NEW.orderdate < DATE '2004-05-01' ) THEN
        INSERT INTO orders_2004_04 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-03-01' AND
         NEW.orderdate < DATE '2004-04-01' ) THEN
        INSERT INTO orders_2004_03 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-02-01' AND
         NEW.orderdate < DATE '2004-03-01' ) THEN
        INSERT INTO orders_2004_02 VALUES (NEW.*);
    ELSIF ( NEW.orderdate >= DATE '2004-01-01' AND
         NEW.orderdate < DATE '2004-02-01' ) THEN
        INSERT INTO orders_2004_01 VALUES (NEW.*);
    ELSE
        RAISE EXCEPTION 'Error in orders_insert_trigger():  date out of range';
    END IF;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_orders_trigger
    BEFORE INSERT ON orders
    FOR EACH ROW EXECUTE PROCEDURE orders_insert_trigger();

--
--  Show structure after adding the empty partitions
--

ANALYZE;
EXPLAIN SELECT * FROM orders;
EXPLAIN ANALYZE SELECT * FROM orders;

-- Order date change update trigger
-- Single sample included and then erased immediately so it doesn't do
-- anything.  Would want one of these on every partition if
-- depending on this to work in production

CREATE OR REPLACE FUNCTION orders_2004_01_update_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF ( NEW.orderdate != OLD.orderdate ) THEN
      DELETE FROM orders_2004_01
        WHERE OLD.orderid=orderid;
      INSERT INTO orders values(NEW.*);
    END IF;
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;
CREATE TRIGGER update_orders_2004_01
     BEFORE UPDATE ON orders_2004_01
     FOR EACH ROW
     EXECUTE PROCEDURE orders_2004_01_update_trigger();

DROP TRIGGER update_orders_2004_01 ON orders_2004_01;
DROP FUNCTION orders_2004_01_update_trigger();

--
-- Migration to partition structure
--

BEGIN;
CREATE OR REPLACE FUNCTION orders_update_trigger()
RETURNS TRIGGER AS $$
BEGIN
    DELETE FROM orders WHERE OLD.orderid=orderid;
    INSERT INTO orders values(NEW.*);
    RETURN NULL;
END;
$$
LANGUAGE plpgsql;
CREATE TRIGGER update_orders
     BEFORE UPDATE ON orders
     FOR EACH ROW
     EXECUTE PROCEDURE orders_update_trigger();

-- Count before
SELECT count(*) FROM orders;
SELECT count(*) FROM orders_2004_01;
SELECT count(*) FROM orders_2004_12;

UPDATE orders SET orderid=orderid;

-- Count after
SELECT count(*) FROM orders;
SELECT count(*) FROM orders_2004_01;
SELECT count(*) FROM orders_2004_12;

-- When totals and partition counts make sense, commit
COMMIT;

--
-- Basic test to prove migration successful
--

ANALYZE;
EXPLAIN ANALYZE SELECT * FROM orders;

--
-- Remove migration trigger
--

DROP TRIGGER update_orders ON orders;
DROP FUNCTION orders_update_trigger();

--
-- Query tests
--

SHOW constraint_exclusion;

EXPLAIN ANALYZE SELECT * FROM orders WHERE orderdate='2004-11-16';
EXPLAIN ANALYZE SELECT * FROM orders WHERE orderid<2000;

